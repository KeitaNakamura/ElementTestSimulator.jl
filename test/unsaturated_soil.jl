check(::Type{Val{:drained_exhausted}}, v0, Sr, dϵ, dSr, ua, dua) = nothing

function check(::Type{Val{:undrained_exhausted}}, v0, Sr, dϵ, dSr, ua, dua)
    de = -v0 * trace(dϵ)
    @test isapprox((v0-1)*dSr + de*Sr, 0.0, atol=TOL)
end

function check(::Type{Val{:undrained_unexhausted}}, v0, Sr, dϵ, dSr, ua, dua)
    de = -v0 * trace(dϵ)
    @test isapprox(ua*de + (v0-1)*(1-Sr)*dua, 0.0, atol=TOL)
end

function check(::Type{Val{:stress_constant}}, dσ, dϵ)
    @test isapprox(dσ[1], 0.0, atol=TOL)
    @test isapprox(dσ[2], 0.0, atol=TOL)
    @test isapprox(dσ[3], 0.0, atol=TOL)
end

function check(::Type{Val{:water_pressure}}, duw, dua, dSr, given)
    @test isapprox(duw, given, atol=TOL)
end

function check(::Type{Val{:air_pressure}}, duw, dua, dSr, given)
    @test isapprox(dua, given, atol=TOL)
end

function check(::Type{Val{:saturation}}, duw, dua, dSr, given)
    @test isapprox(dSr, given, atol=TOL)
end


@testset "Unsaturated soil experiments" begin
    δ = one(SymmetricTensor{2,3})
    ∂σ∂s = 2one(SymmetricTensor{2,3})
    ∂Sr∂s = 2.0
    ∂Sr∂ϵv = 3.0
    v0 = 2.0
    Sr = 0.5
    ua = 20.0
    @testset "$cond" for cond in (:isotropic, :one_dimensional, :volume_constant, :radial_constant, :mean_constant)
        @testset "$ctrl" for ctrl in (:axial_strain, :axial_stress, :deviatoric_stress, :deviatoric_strain)
            if cond == :isotropic && (ctrl == :deviatoric_stress || ctrl == :deviatoric_strain)
                continue
            end
            @testset "$wa" for wa in (:drained_exhausted, :undrained_exhausted, :undrained_unexhausted)
                expt = UnsatExperiment(cond"$cond - $wa - $ctrl", 10.0, 1)
                dσ, dϵ, duw, dua, dSr = expt(D, ∂σ∂s, ∂Sr∂s, ∂Sr∂ϵv, v0, v0, Sr, ua)
                @test isapprox(dσ, D ⊡ dϵ - ∂σ∂s*duw + (∂σ∂s+δ)*dua, atol=TOL)
                @test isapprox(dSr, ∂Sr∂s*(dua-duw) + ∂Sr∂ϵv*trace(dϵ), atol=TOL)
                checktriaxial(dσ, dϵ)
                check(cond, dσ, dϵ)
                check(ctrl, dσ, dϵ, 10.0)
                check(wa, v0, Sr, dϵ, dSr, ua, dua)
                @test eachstep(expt) == 1:1
            end
        end
    end
    @testset "$cond" for cond in (:stress_constant,)
        @testset "$ctrl" for ctrl in (:water_pressure, :air_pressure, :saturation)
            expt = UnsatExperiment(cond"$cond - $ctrl", 10.0, 1)
            dσ, dϵ, duw, dua, dSr = expt(D, ∂σ∂s, ∂Sr∂s, ∂Sr∂ϵv, v0, v0, Sr, ua)
            @test isapprox(dσ, D ⊡ dϵ - ∂σ∂s*duw + (∂σ∂s+δ)*dua, atol=TOL)
            @test isapprox(dSr, ∂Sr∂s*(dua-duw) + ∂Sr∂ϵv*trace(dϵ), atol=TOL)
            checktriaxial(dσ, dϵ)
            check(cond, dσ, dϵ)
            check(ctrl, duw, dua, dSr, 10.0)
            @test eachstep(expt) == 1:1
        end
    end
end
