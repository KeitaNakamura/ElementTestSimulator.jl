@testset "Basic experiments" begin
    δ = one(SymmetricTensor{2,3})
    V, W = δ, 2δ
    @testset "$cond" for cond in (:isotropic, :one_dimensional, :volume_constant, :radial_constant, :mean_constant)
        @testset "$ctrl" for ctrl in (:axial_strain, :axial_stress, :deviatoric_strain, :deviatoric_stress)
            if cond == :isotropic && (ctrl == :deviatoric_stress || ctrl == :deviatoric_strain)
                continue
            end
            expt = Experiment(cond"$cond - $ctrl", 10.0, 1)
            σ, ϵ = expt(D)
            @test isapprox(σ, D ⊡ ϵ, atol=TOL)
            checktriaxial(σ, ϵ)
            check(cond, σ, ϵ)
            check(ctrl, σ, ϵ, 10.0)
            @test eachstep(expt) == 1:1

            expt = Experiment(cond"$cond - $ctrl", (10.0,20.0,30.0), 1)
            σ, ϵ = expt(D, V, W)
            @test isapprox(σ, D ⊡ ϵ + 20.0V + 30.0W, atol=TOL)
            checktriaxial(σ, ϵ)
            check(cond, σ, ϵ)
            check(ctrl, σ, ϵ, 10.0)
            @test eachstep(expt) == 1:1
        end
    end

    # simple shear test
    @testset "simple shear test" begin
        expt = Experiment(cond"volume_constant - shear_stress", 10.0, 1)
        σ, ϵ = expt(D)
        @test isapprox(σ, D ⊡ ϵ, atol=TOL)
        check(Val{:volume_constant}, σ, ϵ)
        @test isapprox(σ[1,2], 10.0, atol=TOL)
        @test isapprox(σ[2,3],  0.0, atol=TOL)
        @test isapprox(σ[1,3],  0.0, atol=TOL)
        @test isapprox(ϵ[2,3],  0.0, atol=TOL)
        @test isapprox(ϵ[1,3],  0.0, atol=TOL)
        @test isapprox(σ[2,2], σ[3,3], atol=TOL)
        @test isapprox(ϵ[2,2], ϵ[3,3], atol=TOL)
        @test eachstep(expt) == 1:1
    end
end
