using ElementTestSimulator
using Tensors
using Base.Test


const TOL = 1e-10


function checktriaxial(σ, ϵ)
    @test isapprox(σ[2,3], 0.0, atol=TOL)
    @test isapprox(σ[1,3], 0.0, atol=TOL)
    @test isapprox(σ[1,2], 0.0, atol=TOL)
    @test isapprox(ϵ[2,3], 0.0, atol=TOL)
    @test isapprox(ϵ[1,3], 0.0, atol=TOL)
    @test isapprox(ϵ[1,2], 0.0, atol=TOL)
    @test isapprox(σ[2,2], σ[3,3], atol=TOL)
    @test isapprox(ϵ[2,2], ϵ[3,3], atol=TOL)
end


check(cond::Symbol, args...) = check(Val{cond}, args...)

function check(::Type{Val{:axial_stress}}, σ, ϵ, σ1)
    @test isapprox(σ[1,1], σ1, atol=TOL)
end

function check(::Type{Val{:axial_strain}}, σ, ϵ, ϵ1)
    @test isapprox(ϵ[1,1], ϵ1, atol=TOL)
end

function check(::Type{Val{:deviatoric_stress}}, σ, ϵ, q)
    @test isapprox(σ[1,1]-σ[3,3], q, atol=TOL)
end

function check(::Type{Val{:deviatoric_strain}}, σ, ϵ, ϵd)
    @test isapprox(ϵ[1,1]-ϵ[3,3], ϵd, atol=TOL)
end


function check(::Type{Val{:isotropic}}, σ, ϵ)
    @test isapprox(σ[1,1], σ[2,2], atol=TOL)
    @test isapprox(σ[1,1], σ[3,3], atol=TOL)
end

function check(::Type{Val{:one_dimensional}}, σ, ϵ)
    @test isapprox(ϵ[2,2], 0.0, atol=TOL)
    @test isapprox(ϵ[3,3], 0.0, atol=TOL)
end

function check(::Type{Val{:volume_constant}}, σ, ϵ)
    @test isapprox(trace(ϵ), 0.0, atol=TOL)
end

function check(::Type{Val{:mean_constant}}, σ, ϵ)
    @test isapprox(trace(σ)/3, 0.0, atol=TOL)
end

function check(::Type{Val{:radial_constant}}, σ, ϵ)
    @test isapprox(σ[2,2], 0.0, atol=TOL)
    @test isapprox(σ[3,3], 0.0, atol=TOL)
end


const D = let
    δ = one(SymmetricTensor{2,3})
    Isym = one(SymmetricTensor{4,3})
    K = 3000.0
    μ = 1500.0
    K*δ⊗δ + 2μ*(Isym - δ⊗δ/3)
end


include("basic.jl")
include("unsaturated_soil.jl")
