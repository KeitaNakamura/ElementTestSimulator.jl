function simulate{Cond, UnsatCond, Ctrl}(::Type{Tuple{Cond, UnsatCond, Ctrl}}, ∂σ∂ϵ::Matrix, ∂σ∂s::Vector, ∂Sr∂s::Real, ∂Sr∂ϵ::Vector, X::Vector, Sr::Real, ua::Real, given::Real)
    n = size(∂σ∂ϵ,1)
    δ = [1.0,1.0,1.0,0.0,0.0,0.0][1:n]
    us = VariableSet(VectorVariable{:dσ}(n), VectorVariable{:dϵ}(n), ScalarVariable{:duw}(), ScalarVariable{:dua}(), ScalarVariable{:dSr}())
    base = @equation dσ dϵ duw dua dSr begin
        dσ == ∂σ∂ϵ*dϵ - ∂σ∂s*duw + (∂σ∂s+δ)*dua
        dSr == ∂Sr∂s*(dua-duw) + ∂Sr∂ϵ'*dϵ
    end
    cond = condition(Val{Cond})
    unsat_cond = unsat_condition(Val{UnsatCond}, ∂σ∂s::Vector, ∂Sr∂s::Real, ∂Sr∂ϵ::Vector, X::Vector, Sr::Real, ua::Real)
    ctrl = control(Val{Ctrl}, given, n)
    system = LinearSystem((base..., cond..., unsat_cond..., ctrl...), us)
    linsolve(system)
end

function simulate{Cond, Ctrl}(::Type{Tuple{Cond, Ctrl}}, ∂σ∂ϵ::Matrix, ∂σ∂s::Vector, ∂Sr∂s::Real, ∂Sr∂ϵ::Vector, X::Vector, Sr::Real, ua::Real, given::Real)
    n = size(∂σ∂ϵ,1)
    δ = [1.0,1.0,1.0,0.0,0.0,0.0][1:n]
    us = VariableSet(VectorVariable{:dσ}(n), VectorVariable{:dϵ}(n), ScalarVariable{:duw}(), ScalarVariable{:dua}(), ScalarVariable{:dSr}())
    base = @equation dσ dϵ duw dua dSr begin
        dσ == ∂σ∂ϵ*dϵ - ∂σ∂s*duw + (∂σ∂s+δ)*dua
        dSr == ∂Sr∂s*(dua-duw) + ∂Sr∂ϵ'*dϵ
    end
    cond = condition(Val{Cond})
    ctrl = control(Val{Ctrl}, given, n)
    system = LinearSystem((base..., cond..., ctrl...), us)
    linsolve(system)
end

# Conditions
# ----------

function condition(::Type{Val{:stress_constant}})
    @equation dσ dσ[1] == dσ[2] == dσ[3] == 0.0
end

# Unsaturated conditions
# ----------------------

function unsat_condition(::Type{Val{:drained_exhausted}}, ∂σ∂s::Vector, ∂Sr∂s::Real, ∂Sr∂ϵ::Vector, X::Vector, Sr::Real, ua::Real)
    @equation duw dua duw == dua == 0.0
end

function unsat_condition(::Type{Val{:undrained_exhausted}}, ∂σ∂s::Vector, ∂Sr∂s::Real, ∂Sr∂ϵ::Vector, X::Vector, Sr::Real, ua::Real)
    @equation dϵ duw dua begin
        (Sr*X + ∂Sr∂ϵ)'*dϵ - ∂Sr∂s*duw + ∂Sr∂s*dua == 0.0
        dua == 0.0
    end
end

function unsat_condition(::Type{Val{:undrained_unexhausted}}, ∂σ∂s::Vector, ∂Sr∂s::Real, ∂Sr∂ϵ::Vector, X::Vector, Sr::Real, ua::Real)
    @equation dϵ duw dua begin
        (Sr*X + ∂Sr∂ϵ)'*dϵ - ∂Sr∂s*duw + ∂Sr∂s*dua == 0.0
        (X*ua)'*dϵ + (1-Sr)*dua == 0.0
    end
end

# Controls
# --------

function control(::Type{Val{:water_pressure}}, given::Real, n::Int)
    @equation dσ duw dua begin
        duw == given
        dua == 0.0
        dσ[4:n] == 0.0
    end
end

function control(::Type{Val{:air_pressure}}, given::Real, n::Int)
    @equation dσ duw dua begin
        duw == 0.0
        dua == given
        dσ[4:n] == 0.0
    end
end

function control(::Type{Val{:saturation}}, given::Real, n::Int)
    @equation dσ dua dSr begin
        dSr == given
        dua == 0.0
        dσ[4:n] == 0.0
    end
end
