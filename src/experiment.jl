abstract type AbstractExperiment end

@inline eachstep(expt::AbstractExperiment) = 1:expt.steps

macro cond_str(str::String)
    symbols = map(x -> Expr(:quote, x), parse.(split(str, "-")))
    return Expr(:curly, :Tuple, symbols...)
end


struct Experiment{C <: Tuple, T <: Union{Real, Tuple}} <: AbstractExperiment
    increments::T
    steps::Int
end

function Experiment(conds::Type{C}, inc::Union{Real, Tuple} = 0.0, steps::Int = 5000) where {C <: Tuple}
    return Experiment{conds,typeof(inc)}(inc, steps)
end

function (expt::Experiment{C})(D::Matrix) where C
    simulate(C, D, expt.increments / expt.steps)
end

function (expt::Experiment{C})(D::Matrix, V::Vector...) where C
    @assert length(expt.increments) == length(V) + 1
    dinc = map(x -> x / expt.steps, expt.increments)
    ext = zeros(size(D,1))
    for (c, v) in zip(dinc[2:end], V)
        @. ext -= c * v
    end
    simulate(C, D, dinc[1], ext)
end

function (expt::Experiment)(D::SymmetricTensor{4,3})
    dσ, dϵ = expt(tovoigt(D))
    fromvoigt(SymmetricTensor{2,3}, dσ), fromvoigt(SymmetricTensor{2,3}, dϵ, offdiagscale = 2.0)
end

function (expt::Experiment)(D::SymmetricTensor{4,3}, V::SymmetricTensor{2,3}...)
    dσ, dϵ = expt(tovoigt(D), map(tovoigt, V)...)
    fromvoigt(SymmetricTensor{2,3}, dσ), fromvoigt(SymmetricTensor{2,3}, dϵ, offdiagscale = 2.0)
end


struct UnsatExperiment{C} <: AbstractExperiment
    increments::Float64
    steps::Int
end

function UnsatExperiment(conds::Type{C}, inc::Real = 0.0, steps::Int = 5000) where {C <: Tuple}
    return UnsatExperiment{conds}(inc, steps)
end

function (expt::UnsatExperiment{C})(∂σ∂ϵ::Matrix, ∂σ∂s::Vector, ∂Sr∂s::Real, ∂Sr∂ϵv::Real, v::Real, v0::Real, Sr::Real, ua::Real) where {C}
    n = size(∂σ∂ϵ, 1)
    δ = [1.0,1.0,1.0,0.0,0.0,0.0][1:n]
    X = -v0 / (v-1) * δ
    ∂Sr∂ϵ = ∂Sr∂ϵv * δ
    simulate(C, ∂σ∂ϵ, ∂σ∂s, ∂Sr∂s, ∂Sr∂ϵ, X, Sr, ua, expt.increments/expt.steps)
end

function (expt::UnsatExperiment)(∂σ∂ϵ::SymmetricTensor{4,3}, ∂σ∂s::SymmetricTensor{2,3}, ∂Sr∂s::Real, ∂Sr∂ϵv::Real, v::Real, v0::Real, Sr::Real, ua::Real)
    dσ, dϵ, duw, dua, dSr = expt(tovoigt(∂σ∂ϵ), tovoigt(∂σ∂s), ∂Sr∂s, ∂Sr∂ϵv, v, v0, Sr, ua)
    fromvoigt(SymmetricTensor{2,3}, dσ), fromvoigt(SymmetricTensor{2,3}, dϵ, offdiagscale = 2.0), duw, dua, dSr
end
