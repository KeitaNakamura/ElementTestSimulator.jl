__precompile__()

module ElementTestSimulator

using LinearSystems
using Tensors
using Compat

using LinearSystems: ScalarVariable, VectorVariable, VariableSet

export Experiment,
       UnsatExperiment,
       eachstep,
       @cond_str

include("experiment.jl")
include("basic.jl")
include("unsaturated_soil.jl")

end # module
