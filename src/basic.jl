function simulate(::Type{Tuple{Cond, Ctrl}}, D::Matrix, given::Real, ext=0.0) where {Cond, Ctrl}
    n = size(D, 1)
    us = VariableSet(VectorVariable{:dσ}(n), VectorVariable{:dϵ}(n))
    base = @equation dσ dϵ begin
        D * dϵ == dσ + ext
    end
    cond = condition(Val{Cond})
    ctrl = control(Val{Ctrl}, given, n)
    system = LinearSystem((base, cond..., ctrl...), us)
    linsolve(system)
end

# Conditions
# ----------

function condition(::Type{Val{:isotropic}})
    @equation dσ dσ[1] == dσ[2] == dσ[3]
end

function condition(::Type{Val{:one_dimensional}})
    @equation dϵ dϵ[2] == dϵ[3] == 0.0
end

function condition(::Type{Val{:volume_constant}})
    @equation dϵ begin
        dϵ[1] + dϵ[2] + dϵ[3] == 0.0
        dϵ[2] == dϵ[3]
    end
end

function condition(::Type{Val{:mean_constant}})
    @equation dσ begin
        dσ[1] + dσ[2] + dσ[3] == 0.0
        dσ[2] == dσ[3]
    end
end

function condition(::Type{Val{:radial_constant}})
    @equation dσ dσ[2] == dσ[3] == 0.0
end

# Controls
# --------

function control(::Type{Val{:axial_stress}}, given::Real, n::Int)
    @equation dσ begin
        dσ[1] == given
        dσ[4:n] == 0.0
    end
end

function control(::Type{Val{:axial_strain}}, given::Real, n::Int)
    @equation dσ dϵ begin
        dϵ[1] == given
        dσ[4:n] == 0.0
    end
end

function control(::Type{Val{:deviatoric_stress}}, given::Real, n::Int)
    @equation dσ begin
        dσ[1] - dσ[3] == given
        dσ[4:n] == 0.0
    end
end

function control(::Type{Val{:shear_stress}}, given::Real, n::Int) # only for volume_constant?
    @equation dσ dϵ begin
        dσ[4:5] == 0.0
        dσ[6] == given
        dϵ[1] == 0.0
    end
end

function control(::Type{Val{:deviatoric_strain}}, given::Real, n::Int)
    @equation dσ dϵ begin
        dϵ[1] - dϵ[3] == given
        dσ[4:n] == 0.0
    end
end
